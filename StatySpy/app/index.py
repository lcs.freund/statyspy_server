from flask_appbuilder import IndexView

class StatySpy_IndexView(IndexView):
    index_template = "statyspy_index.html"