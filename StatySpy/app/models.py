# pylint: disable=C0103
""" MODEL DEFINITION FOR STATYSPY """
import json
import datetime
from flask_appbuilder import Model
from flask_appbuilder.models.mixins import AuditMixin
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, func, Float, Boolean
from sqlalchemy.orm import relationship
from flask import jsonify
"""

You can use the extra Flask-AppBuilder fields and Mixin's

AuditMixin will add automatic timestamp of created and modified by who


Notes:
    One Client Belongs to an User, and an User has many Clients

"""


class Client(Model, AuditMixin):
    """ This class represents a client """
    __tablename__ = "clients"

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    ip_address = Column(String(255))

    cpu_load = Column(Float, default=0.0)
    harddisk_load = Column(Float, default=0.0)
    memory_load = Column(Float, default=0.0)

    auxillary_data = Column(String(), nullable=True)


    def time_since_last_heartbeat(self):
        return datetime.datetime.now() - self.changed_on

    def get_aux_as_dict(self):
        try:
            return json.loads(self.auxillary_data)
        except ValueError as e:
            print(str(e))
            return {"message":"Auxillary Data seems to be no valid json",
                    "severity":"error"}

    def set_aux(self, aux):
        try:
            json.loads(aux)
            self.auxillary_data = aux
            return True
        except ValueError as e:
            print(str(e))
            return False


    def as_dict(self):
        """ returns a dictionary of the objects state """
        ret = {}
        ret['id'] = self.id
        ret['name'] = self.name
        ret['ip_address'] = self.ip_address
        ret['cpu_load'] = self.cpu_load
        ret['memory_load'] = self.memory_load
        ret['harddisk_load'] = self.harddisk_load
        services = []
        for service in self.registered_services:
            services.append(service.as_dict())
        ret['services'] = services
        ret['auxillary_data'] =  json.loads(self.auxillary_data)
        return ret

    def update(self, json_data):
        """ sets values from json onto a client object """
        try:
            self.name = json_data['name']
            self.ip_address = json_data['ip_address']
            self.cpu_load = json_data['cpu_load']
            self.memory_load = json_data['memory_load']
            self.harddisk_load = json_data['harddisk_load']
            self.set_aux(json_data['auxillary_data'])
            return True

        except Exception as e:
            print("One or more keys are missing " +str(e))
            return False


    def from_json(self, json_data, db):
        """ populates fields from json into passed db """
        if self.update(json_data):
            self.save(db)
        else:
            print("Client could not be Updated")

    def save(self, db):
        """ saves the client """
        db.session.add(self)
        db.session.commit()

    # One To Many Relationship. clients can have many services running
    registered_services = relationship(
        "Service", backref="clients", lazy=True, cascade="all, delete-orphan")

    def __repr__(self):
        return "<Client: {}>".format(self.name)


class Service(Model, AuditMixin):
    """ This class represents any watched service on host """
    __tablename__ = "services"
    id = Column(Integer, primary_key=True)
    name = Column(String(80), nullable=False)
    running = Column(Boolean)

    host_id = Column(Integer, ForeignKey(
        'clients.id'), nullable=False)

    def update(self, running, db):
        """ updates the service and saves it to database.
            prints a meaningful message
        """
        self.running = running
        print("Updating {}, running : {} on Host {}".format(self.name, self.running, self.host_id))
        db.session.add(self)
        db.session.commit()

    def as_dict(self):
        """ returns the service as dict"""
        ret = {}
        ret['id'] = self.id
        ret['name'] = self.name
        ret['running'] = self.running
        return ret

    def __repr__(self):
        return '<Service of Host:{} , Name:{} , Running?: {} >'.format(
            self.host_id,
            self.name,
            self.running
        )
