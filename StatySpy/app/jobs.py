# pylint: disable=E1101  
from app import app, db, models
from datetime import timedelta

def alert_on_missing_heartbeat():
    with app.app_context():
        clients = db.session.query(models.Client).all()
        for c in clients:
            if c.time_since_last_heartbeat() > timedelta(minutes=1) :
                print("ALERT: {} did not send a heartbeat for a minute!".format(c.name))