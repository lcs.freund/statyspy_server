""" Server Routes and Route registration """
import json
import logging
from flask import render_template, request, make_response, jsonify, g
from flask_login import login_user
from flask_appbuilder.models.sqla.interface import SQLAInterface
from flask_appbuilder import ModelView, expose, BaseView, has_access
from app import appbuilder, db
from .models import Client, Service

"""
    Create your Views::


    class MyModelView(ModelView):
        datamodel = SQLAInterface(MyModel)


    Next, register your Views::


    appbuilder.add_view(MyModelView,
                        "My View",
                        icon="fa-folder-open-o",
                        category="My Category",
                        category_icon='fa-envelope'
                        )
"""



"""
Here is an example for filtering a table by the user who created it.
Also it shows the syntax for multiple filters by example

from .models import MyTable

def get_user():
    return g.user

class MyView(ModelView):
    datamodel = SQLAInterface(MyTable)
    base_filters = [['created_by', FilterEqualFunction, get_user],
                    ['name', FilterStartsWith, 'a']]


"""

# disable MAYBE NO MEMBER warning, as query is dynamically generated. also some styles
# i do not agree with
# pylint: disable=E1101
# pylint: disable=C0103
# pylint: disable=C0301
# pylint: disable=R0201
# pylint: disable=W1202
log = logging.getLogger(__name__)

    
# TODO: we should log all the things... 
# TODO: Error handling is virtually non existent. that sucks


class ServiceView(ModelView):
    """ Autoview for services """
    datamodel = SQLAInterface(Service)
    list_columns = ['name', 'running']

class ClientView(ModelView):
    """ Autoview for Clients """
    non_human_editable_columns = [
        'created_on',
        'changed_on',
        'cpu_load',
        'memory_load',
        'harddisk_load',
        'auxillary_data'
    ]
    datamodel = SQLAInterface(Client)

    list_columns = ['id',
                    'name',
                    'ip_address',
                    'cpu_load',
                    'harddisk_load',
                    'memory_load',
                    'auxillary_data']

    # exclude auto filled columns or columns only clients should update
    edit_exclude_columns = non_human_editable_columns.copy()
    add_exclude_columns = non_human_editable_columns.copy()

    related_views = [ServiceView]
    show_template = 'appbuilder/general/model/show_cascade.html'

class ApiLogin(BaseView):
    """ Login View for clients """
    route_base = '/api'

    @expose('/login', methods=['POST'])
    def login(self):
        """ Login the user into rest api """
        json_data = request.get_json()
        log.info(" Login is attempted for {}".format(json_data['username']))

        if g.user is not None and g.user.is_authenticated():
            http_return_code = 401
            response = make_response(jsonify({'message': 'Login Failed already authenticated',
                                              'severity': 'critical'}), http_return_code)
        username = json_data['username']
        password = json_data['password']
        user = self.appbuilder.sm.auth_user_db(username, password)

        if not user:
            log.warning(" Login attempted for {} failed. Wrong Password ".format(json_data['username']))
            http_return_code = 401
            response = make_response(jsonify({'message': 'Login Failed',
                                              'severity': 'critical'}), http_return_code)
        else:
            login_user(user, remember=False)
            http_return_code = 201
            log.info(" Login attempted for {} Succeeded. ".format(json_data['username']))
            response = make_response(jsonify({'message': 'Login Success',
                                              'severity': 'info'}), http_return_code)

        return response


class RestApi(BaseView):
    """ Rest Api Routes """
    route_base = '/api/clients'

    @has_access
    @expose("/update/<int:cl_id>", methods=['PUT'])
    def update(self, cl_id):
        "Updates Client Data"
        json_data = json.loads(request.get_json())
        client = db.session.query(Client).filter_by(id=cl_id).first()
        print(client)
        if client is None or not json_data:
            log.warning("Post request sent insufficient data, or the requested client is non existent") 
            return make_response(jsonify({'message': 'Either you sent insufficient data, or the client does not exist',
                                          'severity': 'info'}), 404)
        # ============ REFACTOR THE SHIT OUT OF THIS ================
        client.from_json(json_data,db)

        for service in json_data['services']:
            running = bool()
            if service['status'] == 'running':
                running = True
            elif service['status'] == 'stopped':
                running = False

            already_registered = bool()
            service_id = int()

            for registered_service in client.registered_services:
                if registered_service.name == service['name']:
                    already_registered = True
                    service_id = registered_service.id

            if not already_registered:
                log.info("Registering new service: {}".format(service['name']))
                database_service = Service()
                database_service.name = service['name']
                database_service.host_id = client.id

            else:
                database_service = db.session.query(
                    Service).filter_by(id=service_id).first()

            database_service.update(running, db)
            db.session.add(database_service)
        log.info("Client {} updated".format(client.name)) 
        client.save(db)

        return make_response(jsonify({'message': 'Update succeeded',
                                      'severity': 'info'}), 200)

    @has_access
    @expose('/register', methods=['POST'])
    def register(self):
        """
            register a client and return its unique id on the server for persistence.
            users cant register more than one client with the same name.
        """
        json_data = json.loads(request.get_json())

        if g.user is not None and g.user.is_authenticated():
            clients = db.session.query(Client).filter_by(created_by=g.user).all()
            log.warning("Attempt to add Client with an already existing name") 
            for client in clients:
                if client.name == json_data['name']: # User already has a client with this name
                    return make_response(jsonify({'message': 'Client Already Exists for this user',
                                                  'severity': 'critical'}), 401)

            client = Client()
            client.from_json(json_data, db) # Client is new, by its name
            log.info("Client {} registered Successfully".format(client.name)) 
            return make_response(jsonify({'message': 'Client Creation Successfull',
                                          'created_client_id':str(client.id),
                                          'severity': 'info'}), 200)

        # Should not happen....
        return make_response(jsonify({'message': 'Something went terribly wrong',
                                      'severity': 'critical'}), 500)

    @has_access
    @expose('/unregister/<int:client_id>', methods=['DELETE'])
    def unregister(self, client_id):
        """ unregisters a client from the server"""
        if g.user is not None and g.user.is_authenticated():
            client = db.session.query(Client).filter_by(id=client_id).first()
            if not client:
                log.warning("There was an attempt to delete a non existent client") 
                return make_response(jsonify({'message':'No Client with this ID',
                                              'severity':'critical'
                }), 404)

            db.session.delete(client)
            log.info("Deleted Client {}, ID: {}".format(client.name, client.id)) 
            db.session.commit()
            
            return make_response(jsonify({'message': 'Deletion succeeded',
                                          'severity': 'info'
                                          }), 203)

        return make_response(jsonify({'message': 'Either Request was not authenticated or the user has no access to the ressource',
                                      'severity': 'critical'}), 401)

    @has_access
    @expose('/', methods=['GET'])
    def clients(self):
        """
        retreive a list of clients
        """
        clients = db.session.query(Client).filter_by(created_by=g.user).all()
        data = []
        for client in clients:
            data.append(client.as_dict())

        return make_response(jsonify({'message':'Retrieval Successfull',
                                      'data': data,
                                      'severity': 'info'}), 200)

    @has_access
    @expose('/<int:cl_id>', methods=['GET'])
    def client_by_id(self, cl_id):
        """
        Retreive a client by the given id
        """
        client = db.session.query(Client).filter_by(id=cl_id).first()
        data = client.as_dict()
        return make_response(jsonify({'message':'Retrieval Successfull',
                                      'data': data,
                                      'severity': 'info'}), 200)

"""
    Application wide 404 error handle
"""


@appbuilder.app.errorhandler(404)
def page_not_found(e):
    """ app wide 404 handler """
    return render_template('404.html', base_template=appbuilder.base_template, appbuilder=appbuilder, e=e), 404


db.create_all()
appbuilder.add_view(ClientView, "Clients",
                    icon="fa_folder_open_o", category="Clients")

appbuilder.add_view_no_menu(ServiceView, "ServiceView")
appbuilder.add_view_no_menu(ApiLogin, "ApiLogin")
appbuilder.add_view_no_menu(RestApi, "RestApi")
