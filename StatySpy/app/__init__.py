import logging

from flask import Flask
from flask_appbuilder import SQLA, AppBuilder
from flask_apscheduler import APScheduler



"""
 Logging configuration
"""

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logging.getLogger().setLevel(logging.DEBUG)


# == INIT APP AND READ CONFIG
app = Flask(__name__)
app.config.from_object('config')


# == INITIALIZE DATABASE
db = SQLA(app)


# == INITIALIZE CRONJOBS
def testjob(a,b):
    print(" Ich bin ein getimter testjob....")
    print(a + b)



from .jobs import alert_on_missing_heartbeat as alert_heartbeat
app.config['JOBS'] = list()
app.config['JOBS'].append({
            'id':'heartbeat',
            'func':alert_heartbeat,
            'trigger':'interval',
            'seconds': 5
        })

scheduler = APScheduler()
scheduler.init_app(app)
scheduler.start()

# == INITIALIZE APPBUILDER
from app.index import StatySpy_IndexView
appbuilder = AppBuilder(app, db.session, indexview=StatySpy_IndexView)




"""
from sqlalchemy.engine import Engine
from sqlalchemy import event

#Only include this for SQLLite constraints
@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    # Will force sqllite contraint foreign keys
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()
"""    
from app import views


