# StatySpy Monitor Server

StatySpy Server collects Data sent to it from one or more computers running the client application which runs on Linux and Windows. You can find it [here](https://gitlab.com/lcs.freund/statyspy_client)



Please note that this software is not ready for use in production. 

Find steps to get a test and development environment up and running below.

### Features
    - receive stats from clients via rest api. 
    - frontend allows admin users to view their clients ( this feature needs to be
      reworked)

### Features in development
    - dashboard based on React JS ( right now you can only view stats in database view )
    - two way communication between client and server


## Installation:
1. Extract downloaded ZIP or clone repository.

2. Create a virtual python environment, activate it and install dependencies:
    
    ```bash
    python virtualenv venv
    ./venv/bin/activate
    pip install -r requirements.txt
    ``` 

3. Initialize database and create an admin user ( this utilizes the flask app builder mananger ):

    ```bash
    cd StatySpy
    fabmanager create-admin
    ```

4. The application should create an sqlite database on first start if none exists.
In case this did not work for you, you need to create it manually or use flask app builders built in command:

    ```bash
    # execute inside StatySpy/ directory
    fabmanager run
    # if there is an error with the database please try the following
    fabmanager create-db
    ```


### DONE!
Your Statyspy DEV server is now listening for data.
If you would like to know how to get the client application running, see [here](https://gitlab.com/lcs.freund/statyspy_client)






